# Dgenies docker

## Dgenies in standalone mode

### With docker

```{bash}
git clone https://forgemia.inra.fr/genotoul-bioinfo/dgenies-docker.git
cd dgenies-docker/dgenies
docker build --target dgenies-standalone -t dgenies .
docker run --rm dgenies
```

#### Mounting your `config` and `workdir` volumes

You can mount volumes such a way. Environment variables `USER_ID` and `GROUP_ID` are used to map dgenies user in container to current local host user.

```{bash}
# We assume you're still in 'dgenies-docker/dgenies' from previous step
docker run \
    -v "${PWD}"/volumes/config:/etc/dgenies \
    -v "${PWD}"/volumes/workdir:/dgenies \
    -v "${PWD}"/volumes/example:/dgenies/example \
    -e "USER_ID=$(id -u)" \
    -e "GROUP_ID=$(id -g)" \
    dgenies
```

##### Passing environment variables

Alternatively, you can create a file containning environment variables to be pass to docker container when run.

For example, we can create an env file `dgenies.env` with following command:

```{bash}
echo "
USER_ID=$(id -u)
GROUP_ID=$(id -g)
" > ../dgenies.env
```

and, then run dgenies with this command line:

```{bash}
docker run \
    -v "${PWD}"/docker/config:/etc/dgenies \
    -v "${PWD}"/docker/workdir:/dgenies \
    --env-file ../dgenies.env
    dgenies
```

### With docker compose

At project root (i.e. `dgenies-docker`), create:

1. an env file as in [previous subsection](#passing-environment-variables),
2. and an additional file named [`docker-compose.override.yml` or `composse-override.yml`](https://docs.docker.com/compose/extends/#multiple-compose-files) with following content:

```
version: '3.9'

services:

  dgenies-standalone:
    env_file:
      dgenies.env

  dgenies-webserver:
    env_file:
      dgenies.env
```

Check and configure `compose.yml` file, in particular volumes part, end then start the containers

```{bash}
# We (re)build the images
docker-compose --profile standalone build
# We start the containers
docker-compose --profile standalone up
# We stop and destroy the containers
docker-compose --profile standalone down
```

## Dgenies in webserver mode with flask

Check and update database login and password in either dgenies config file and docker `compose.yml` file. You can use the `docker-compose.override.yml` or `dgenies.env` file to overwrite the default ones set in `compose.yml` file.

When configured, you just need to change the profile used by `docker-compose`

```{bash}
docker-compose --profile webserver up
```


## Dgenies in webserver mode with gunicorn

Check and update database login and password in either dgenies config file and docker `compose.yml` file. You can use the `docker-compose.override.yml` or `dgenies.env` file to overwrite the default ones set in `compose.yml` file.

When configured, you just need to change the profile used by `docker-compose`

```{bash}
docker-compose --profile webserver-gunicorn up
```


## Dgenies in clister mode (WIP)

```{bash}
docker-compose --profile drmaa up
```
