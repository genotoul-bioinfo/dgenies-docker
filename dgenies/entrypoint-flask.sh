#!/bin/bash

# Set USER_ID to DEFAULT_DGENIES_USER_ID (999) for dgenies if not defined
USER_ID=${USER_ID:-${DEFAULT_DGENIES_USER_ID}}
GROUP_ID=${GROUP_ID:-${DEFAULT_DGENIES_GROUP_ID}}

# if [[ -z ${USER_ID} ]]; then
#   USER_ID="$(id -u dgenies)"
# fi

# id -u dgenies

# if [[ -z ${GROUP_ID} ]]; then
#   GROUP_ID="$(id -g dgenies)"
# fi

# # TODO: avoid automatic home permission correction
# usermod -u "${USER_ID}" dgenies
# groupmod -g "${GROUP_ID}" dgenies

# We create dgenies user and group on the fly when container is run
echo "Starting with UID:GID : ${USER_ID}:${GROUP_ID}"
groupadd -r -g "${GROUP_ID}" dgenies
useradd --no-log-init -d /dgenies -r -u "${USER_ID}" -g dgenies dgenies

# We start the local scheduler if needed (webserver mode)
if [ -f "/start_local_scheduler.sh" ]; then
    DGENIES_PYTHON_PATH=$(dirname "$(python3 -c 'import dgenies; print(dgenies.__file__)')")
    echo "Starting local scheduler"

    # PID way, not really needed with docker
    #exec /usr/sbin/gosu dgenies /start_local_scheduler.sh \
    #    "${DGENIES_PYTHON_PATH}" \
    #    python3 \
    #    /dgenies/.local_scheduler.pid \
    #    /dgenies/logs/
    
    # TODO an entrypoint in python package
    exec /usr/sbin/gosu dgenies python3 \
        "${DGENIES_PYTHON_PATH}/bin/local_scheduler.py" \
        -d "true" &
fi

echo "Starting dgenies"
exec /usr/sbin/gosu dgenies dgenies "$@"