#!/bin/bash

# Set USER_ID to DEFAULT_DGENIES_USER_ID (999) for dgenies if not defined
USER_ID=${USER_ID:-${DEFAULT_DGENIES_USER_ID}}
GROUP_ID=${GROUP_ID:-${DEFAULT_DGENIES_GROUP_ID}}

# We create dgenies user and group on the fly when container is run
echo "Starting with UID:GID : ${USER_ID}:${GROUP_ID}"
groupadd -r -g "${GROUP_ID}" dgenies
useradd --no-log-init -d /dgenies -r -u "${USER_ID}" -g dgenies dgenies

# We start the local scheduler
DGENIES_PYTHON_PATH=$(dirname "$(python3 -c 'import dgenies; print(dgenies.__file__)')")
echo "Starting local scheduler"

# PID way, not really needed with docker
#exec /usr/sbin/gosu dgenies /start_local_scheduler.sh \
#    "${DGENIES_PYTHON_PATH}" \
#    python3 \
#    /dgenies/.local_scheduler.pid \
#    /dgenies/logs/

# TODO an entrypoint in python package
exec /usr/sbin/gosu dgenies python3 \
    "${DGENIES_PYTHON_PATH}/bin/local_scheduler.py" \
    -d "true" &

echo "Starting dgenies with gunicorn"
# TODO: use GUNICORN_CMD_ARGS to pass option with docker
#exec /usr/sbin/gosu dgenies gunicorn \
exec /usr/local/bin/gunicorn \
    --user "${USER_ID}" \
    --group "${GROUP_ID}" \
    --workers=2 \
    --name dgenies \
    --access-logfile /dgenies/logs/access.log \
    --error-logfile /dgenies/logs/errors.log \
    "$@" \
    'dgenies:launch()'
